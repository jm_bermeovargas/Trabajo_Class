﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public string sumar(int n1, int n2){
            return (n1 + n2).ToString();
        }

        public string restar(int n1, int n2){
            return (n1 + n2).ToString();
        }

        public string multiplicar(int n1, int n2){
            return (n1 + n2).ToString();
        }

        public string dividir(int n1, int n2){
            return (n1 + n2).ToString();
        }

        public string raiz (int n){
            return Math.Sqrt(n).ToString();
        }
    }
}
