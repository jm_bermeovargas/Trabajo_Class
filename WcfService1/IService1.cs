﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string sumar(int n1, int n2);
        [OperationContract]
        string restar(int n1, int n2);
        [OperationContract]
        string multiplicar(int n1, int n2);
        [OperationContract]
        string dividir(int n1, int n2);
        [OperationContract]
        string raiz(int n);
    }
  }

